import { Router } from 'express';
import barang from './routes/barang';
import invoices from './routes/invoices';
import supplier from './routes/supplier';
import product from './routes/products';
import auth from './routes/auth';

const router = Router();

router
  .use('/supplier', supplier)
  .use('/barang', barang)
  .use('/invoices', invoices)
  .use('/product', product)
  .use('/auth', auth);

export default router;
