import { Router } from 'express';
import utils from '../helpers/utils';
import authCont from '../controllers/auth';
import validator from '../helpers/validator';

const router = Router();

router
  .post('/login', utils.access, authCont.Login)
  .post(
    '/register',
    validator.ruleRegister(),
    validator.validate,
    utils.access,
    authCont.Register,
  );

export default router;
