import { Router } from 'express';
import utils from '../helpers/utils';
import barangCont from '../controllers/barang';
import validator from '../helpers/validator';

const router = Router();

router
  .post(
    '/',
    utils.access,
    validator.ruleAddBarang(),
    validator.validate,
    barangCont.Add,
  )
  .get('/', utils.access, barangCont.getAll)
  .get('/:id', utils.access, barangCont.getOne)
  .patch('/:id', utils.access, barangCont.Update)
  .delete('/:id', utils.access, barangCont.Delete);

export default router;
