import { Router } from 'express';
import utils from '../helpers/utils';
import productCont from '../controllers/product';
// import validator from '../helpers/validator';

const router = Router();

router
  .post('/', utils.access, productCont.Add)
  .patch('/:id', utils.access, productCont.Update)
  .delete('/:id', utils.access, productCont.Delete);

export default router;
