import { Router } from 'express';
import utils from '../helpers/utils';
import invCont from '../controllers/invoices';
import validator from '../helpers/validator';

const router = Router();

router
  .post('/', validator.ruleAddInvoice(), validator.validate, utils.access, invCont.Add)
  .get('/', utils.access, invCont.getAll)
  .get('/report', utils.access, invCont.reportGetAll)
  .get('/report/:id', utils.access, invCont.reportGetAll)
  .get('/:id', utils.access, invCont.getOne)
  .patch('/:id', utils.access, invCont.Update)
  .delete('/:id', utils.access, invCont.Delete);

export default router;
