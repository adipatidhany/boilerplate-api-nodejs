import { Router } from 'express';
import utils from '../helpers/utils';
import supplierCont from '../controllers/supplier';
import validator from '../helpers/validator';

const router = Router();

router
  .post(
    '/',
    utils.access,
    validator.ruleAddSupplier(),
    validator.validate,
    supplierCont.Add,
  )
  .get('/', utils.access, supplierCont.getAll)
  .get('/:id', utils.access, supplierCont.getOne)
  .patch('/:id', utils.access, supplierCont.Update)
  .delete('/:id', utils.access, supplierCont.Delete);

export default router;
