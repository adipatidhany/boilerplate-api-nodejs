/* eslint-disable consistent-return */
import bcrypt from 'bcrypt';

require('dotenv').config();

const access = async (req, res, next) => {
  const { token } = req.headers;
  const { key } = process.env;
  if (token === undefined || token !== key) {
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: 'key invalid' });
  }
  next();
};

const generateSalt = () => bcrypt.genSalt(10);

const hashPassword = (password, salt) => bcrypt.hash(password, salt);

const comparePassword = (inputPassword, userPassword) => bcrypt.compare(inputPassword, userPassword);

const utils = { access, generateSalt, hashPassword, comparePassword };

export default utils;
