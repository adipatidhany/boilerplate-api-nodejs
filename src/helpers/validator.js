import { body, validationResult } from 'express-validator';

const ruleAddBarang = () => [
  body('idBarang').notEmpty().withMessage('id of barang cannot empty'),
  body('supplierId').notEmpty().withMessage('id of supplier cannot empty'),
  body('name').notEmpty().withMessage('name of barang cannot empty'),
  body('quantity').notEmpty().withMessage('quantity of barang cannot empty'),
  body('dimension').notEmpty().withMessage('dimension of barang cannot empty'),
];

const ruleAddSupplier = () => [
  body('supplierId').notEmpty().withMessage('id of supplier cannot empty'),
  body('name').notEmpty().withMessage('name of barang cannot empty'),
  body('vendor').notEmpty().withMessage('vendor of barang cannot empty'),
];

const ruleAddInvoice = () => [
  body('invoiceId').notEmpty().withMessage('id of invoice cannot empty'),
  body('name').notEmpty().withMessage('name of barang cannot empty'),
];

const ruleRegister = () => [
  body('name').notEmpty().withMessage('name of user can not empty'),
  body('username').notEmpty().withMessage('username of user can not empty'),
  body('email')
    .notEmpty()
    .withMessage('email of user can not empty')
    .isEmail()
    .withMessage('format email not valid'),
  body('password').notEmpty().withMessage('password of user can not empty'),
  body('confirmPassword')
    .notEmpty()
    .withMessage('confirmPassword of user can not empty')
    .custom((value, { req }) => value === req.body.password)
    .withMessage('password not match'),
  body('role').notEmpty().withMessage('role of user can not empty'),
];

const validate = (req, res, next) => {
  const errors = validationResult(req);
  return errors.isEmpty()
    ? next()
    : res
      .status(400)
      .send({ code: 400, status: 'failed', message: errors.errors[0].msg });
};

const validator = {
  ruleAddBarang,
  ruleRegister,
  validate,
  ruleAddSupplier,
  ruleAddInvoice,
};

export default validator;
