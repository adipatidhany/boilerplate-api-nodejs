import jwt from 'jsonwebtoken';
import moment from 'moment';
import utils from '../helpers/utils';
import authModels from '../models/auth';

require('dotenv').config();

const Register = async (req, res) => {
  const { name, username, email, password, confirmPassword, role } = req.body;
  try {
    const resUser = await authModels.getOne(email, username);
    if (resUser.length > 0) {
      return res
        .status(400)
        .send({
          code: 400,
          message: 'email / username has been used',
          data: [],
        });
    }
    const salt = await utils.generateSalt();
    const hashPassword = await utils.hashPassword(password, salt);
    const dataRegister = {
      name,
      username,
      email,
      salt,
      password: hashPassword,
      role,
    };
    await authModels.Add(dataRegister);
    return res.json({
      code: 200,
      message: 'success register new user',
      data: [],
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, message: error.message, data: [] });
  }
};

const Login = async (req, res) => {
  const { email, password } = req.body;
  try {
    const resUser = await authModels.getOne(email, email);
    if (resUser.length === 0) {
      return res
        .status(400)
        .send({ code: 400, message: 'email / username not found', data: [] });
    }
    const checkPassword = await utils.comparePassword(
      password,
      resUser[0].password,
    );
    if (!checkPassword) {
      return res
        .status(400)
        .send({ code: 400, message: 'password not match', data: [] });
    }
    const dataJWT = {
      username: resUser[0].username,
      role: resUser[0].role,
      expired: moment(new Date()).add(1, 'hour'),
    };
    const token = jwt.sign(dataJWT, process.env.key, { expiresIn: '1h' });
    return res.json({ code: 200, message: 'success login', token });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, message: error.message, data: [] });
  }
};

const authCont = { Register, Login };

export default authCont;
