import moment from 'moment';
import supplierModel from '../models/supplier';

const Add = async (req, res) => {
  const { supplierId, name, vendor } = req.body;
  try {
    const createdAt = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    const resSupplier = await supplierModel.getOne(supplierId);
    if (resSupplier.length > 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'cannot duplicate kode supplier',
          data: [],
        });
    }
    const data = { supplierId, name, vendor };
    await supplierModel.Add(data);
    console.log(createdAt, 'success added new supplier');
    const resOneSupplier = await supplierModel.getOne(supplierId);
    return res.json({
      code: 200,
      status: 'success',
      message: 'success add new supplier',
      data: resOneSupplier,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const Update = async (req, res) => {
  const { id } = req.params;
  const { name, vendor } = req.body;
  try {
    const resSupplier = await supplierModel.getOne(id);
    if (resSupplier.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'kode supplier not found',
          data: [],
        });
    }
    let data = {};
    data = name ? { ...data, name } : { ...data };
    data = vendor ? { ...data, vendor } : { ...data };
    await supplierModel.Update(data, id);
    const resOneSupplier = await supplierModel.getOne(id);
    return res.json({
      code: 200,
      status: 'success',
      message: 'success updated supplier',
      data: resOneSupplier,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const getAll = async (req, res) => {
  try {
    const resSupplier = await supplierModel.getAll();
    if (resSupplier.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'supplier not found',
          data: [],
        });
    }
    return res.json({
      code: 200,
      status: 'success',
      message: 'success get all supplier',
      data: resSupplier,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const getOne = async (req, res) => {
  const { id } = req.params;
  try {
    const resSupplier = await supplierModel.getOne(id);
    if (resSupplier.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'supplier not found',
          data: [],
        });
    }
    return res.json({
      code: 200,
      status: 'success',
      message: 'success get all supplier',
      data: resSupplier,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const Delete = async (req, res) => {
  const { id } = req.params;
  try {
    const resSupplier = await supplierModel.getOne(id);
    if (resSupplier.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'supplier not found',
          data: [],
        });
    }
    await supplierModel.Delete(id);
    return res.json({
      code: 200,
      status: 'success',
      message: 'success deleted one supplier',
      data: [],
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const supplierCont = { Add, Update, getAll, getOne, Delete };

export default supplierCont;
