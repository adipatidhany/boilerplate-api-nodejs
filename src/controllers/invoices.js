import moment from 'moment';
import invoiceModel from '../models/invoices';
import productModel from '../models/product';
import barangModel from '../models/barang';

const Add = async (req, res) => {
  const { invoiceId, name } = req.body;
  try {
    const createdAt = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    const resInvoice = await invoiceModel.getOne(invoiceId);
    if (resInvoice.length > 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'cannot duplicate kode invoice',
          data: [],
        });
    }
    const data = { invoiceId, name };
    await invoiceModel.Add(data);
    console.log(createdAt, 'success added new invoice');
    const resOneInvoice = await invoiceModel.getOne(invoiceId);
    return res.json({
      code: 200,
      status: 'success',
      message: 'success add new invoice',
      data: resOneInvoice,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const getAll = async (req, res) => {
  try {
    const resInvoice = await invoiceModel.getAll();
    if (resInvoice.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'invoice not found',
          data: [],
        });
    }
    return res.json({
      code: 200,
      status: 'success',
      message: 'success get all invoice',
      data: resInvoice,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const getOne = async (req, res) => {
  const { id } = req.params;
  try {
    const resInvoice = await invoiceModel.getOne(id);
    if (resInvoice.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'invoice not found',
          data: [],
        });
    }
    return res.json({
      code: 200,
      status: 'success',
      message: 'success get all invoice',
      data: resInvoice,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const Delete = async (req, res) => {
  const { id } = req.params;
  try {
    const resInvoice = await invoiceModel.getOne(id);
    if (resInvoice.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'invoice not found',
          data: [],
        });
    }
    await invoiceModel.Delete(id);
    return res.json({
      code: 200,
      status: 'success',
      message: 'success deleted one invoice',
      data: [],
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const Update = async (req, res) => {
  const { id } = req.params;
  const { keterangan } = req.body;
  try {
    const resInvoice = await invoiceModel.getOne(id);
    if (resInvoice[0].keterangan === 'Batal' || resInvoice[0].keterangan === 'Diterima') {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: `Status Invoices sudah ${resInvoice[0].keterangan}`,
          data: [],
        });
    }
    if (resInvoice.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'invoice not found',
          data: [],
        });
    }
    const data = { keterangan };
    await invoiceModel.Update(data, id);
    const resOneInvoice = await invoiceModel.getOne(id);
    if (keterangan === 'Batal') {
      const resProduct = await productModel.getInvoiceProduct(id);
      const updateQuantity = resProduct.map(async (e) => {
        const resBarang = await barangModel.getOne(e.barangId);
        const dataUpdateBarang = {
          quantity: resBarang[0].quantity + resProduct[0].quantity,
        };
        console.log(dataUpdateBarang);
        await barangModel.Update(dataUpdateBarang, e.barangId);
      });
      await Promise.all(updateQuantity);
    }
    return res.json({
      code: 200,
      status: 'success',
      message: 'success updated invoice',
      data: resOneInvoice,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const reportGetAll = async (req, res) => {
  try {
    const resInvoice = await invoiceModel.getAll();
    if (resInvoice.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'invoice not found',
          data: [],
        });
    }
    const tempInvoice = resInvoice.map(async (e, i) => {
      const resProduct = await productModel.getInvoiceProduct(e.invoiceId);
      resInvoice[i] = { ...resInvoice[i], product: resProduct };
    });
    await Promise.all(tempInvoice);
    return res.json({
      code: 200,
      status: 'success',
      message: 'success get all invoice',
      data: resInvoice,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const reportGetOne = async (req, res) => {
  const { id } = req.params;
  try {
    const resInvoice = await invoiceModel.getOne();
    if (resInvoice.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'invoice not found',
          data: [],
        });
    }
    const resProduct = await productModel.getInvoiceProduct(resInvoice[0].invoiceId);
    resInvoice[0] = { ...resInvoice[0], product: resProduct };
    return res.json({
      code: 200,
      status: 'success',
      message: 'success get all invoice',
      data: resInvoice,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const invoiceCont = { Add, getAll, getOne, Delete, Update, reportGetAll, reportGetOne };

export default invoiceCont;
