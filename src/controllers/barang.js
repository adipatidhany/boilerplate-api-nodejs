import moment from 'moment';
import barangModel from '../models/barang';

const Add = async (req, res) => {
  const { idBarang, supplierId, name, quantity, dimension } = req.body;
  try {
    const createdAt = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    const resBarang = await barangModel.getOne(idBarang);
    if (resBarang.length > 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'cannot duplicate kode barang',
          data: [],
        });
    }
    const data = { supplierId, idBarang, name, quantity, dimension };
    await barangModel.Add(data);
    console.log(createdAt, 'success added new barang');
    const resOneBarang = await barangModel.getOne(idBarang);
    return res.json({
      code: 200,
      status: 'success',
      message: 'success add new barang',
      data: resOneBarang,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const Update = async (req, res) => {
  const { id } = req.params;
  const { name, quantity } = req.body;
  try {
    const resBarang = await barangModel.getOne(id);
    if (resBarang.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'kode barang not found',
          data: [],
        });
    }
    let data = {};
    data = name ? { ...data, name } : { ...data };
    data = quantity ? { ...data, quantity } : { ...data };
    await barangModel.Update(data, id);
    const resOneBarang = await barangModel.getOne(id);
    return res.json({
      code: 200,
      status: 'success',
      message: 'success updated barang',
      data: resOneBarang,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const getAll = async (req, res) => {
  try {
    const resBarang = await barangModel.getAll();
    if (resBarang.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'barang not found',
          data: [],
        });
    }
    return res.json({
      code: 200,
      status: 'success',
      message: 'success get all barang',
      data: resBarang,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const getOne = async (req, res) => {
  const { id } = req.params;
  try {
    const resBarang = await barangModel.getOne(id);
    if (resBarang.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'barang not found',
          data: [],
        });
    }
    return res.json({
      code: 200,
      status: 'success',
      message: 'success get all barang',
      data: resBarang,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const Delete = async (req, res) => {
  const { id } = req.params;
  try {
    const resBarang = await barangModel.getOne(id);
    if (resBarang.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'barang not found',
          data: [],
        });
    }
    await barangModel.Delete(id);
    return res.json({
      code: 200,
      status: 'success',
      message: 'success deleted one barang',
      data: [],
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const barangCont = { Add, Update, getAll, getOne, Delete };

export default barangCont;
