import moment from 'moment';
import productModel from '../models/product';
import barangModel from '../models/barang';

const Add = async (req, res) => {
  const { invoiceId, barangId, quantity } = req.body;
  try {
    const isExist = await productModel.isBarangExist(invoiceId, barangId);
    if (isExist.length > 0) {
      return res
        .status(400)
        .send({ code: 400, status: 'failed', message: 'cannot duplicate barang', data: [] });
    }
    const resBarang = await barangModel.getOne(barangId);
    const createdAt = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    const data = { invoiceId, barangId, quantity };
    await productModel.Add(data);
    const dataUpdateBarang = {
      quantity: resBarang[0].quantity - quantity,
    };
    console.log('Sisa quantity barang ', barangId, dataUpdateBarang);
    await barangModel.Update(dataUpdateBarang, resBarang[0].idBarang);
    console.log(createdAt, 'success added new product');
    return res.json({
      code: 200,
      status: 'success',
      message: 'success add new product',
      data: [],
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const Update = async (req, res) => {
  const { id } = req.params;
  const { quantity } = req.body;
  try {
    const resProduct = await productModel.getOne(id);
    if (resProduct.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'product not found',
          data: [],
        });
    }
    let data = {};
    data = quantity ? { ...data, quantity } : { ...data };
    await productModel.Update(data, id);
    const resBarang = await barangModel.getOne(resProduct[0].barangId);
    const tempQuantity = resProduct[0].quantity - quantity;
    const dataUpdateBarang = {
      quantity: resBarang[0].quantity + tempQuantity,
    };
    console.log('Sisa quantity barang ', resBarang[0].idBarang, dataUpdateBarang);
    await barangModel.Update(dataUpdateBarang, resBarang[0].idBarang);

    const resOneProduct = await productModel.getOne(id);
    return res.json({
      code: 200,
      status: 'success',
      message: 'success updated barang',
      data: resOneProduct,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const Delete = async (req, res) => {
  const { id } = req.params;
  try {
    const resProduct = await productModel.getOne(id);
    if (resProduct.length === 0) {
      return res
        .status(400)
        .send({
          code: 400,
          status: 'failed',
          message: 'product not found',
          data: [],
        });
    }
    await productModel.Delete(id);
    const resBarang = await barangModel.getOne(resProduct[0].barangId);
    const dataUpdateBarang = {
      quantity: resBarang[0].quantity + resProduct[0].quantity,
    };
    console.log('Sisa quantity barang ', resBarang[0].idBarang, dataUpdateBarang);
    await barangModel.Update(dataUpdateBarang, resBarang[0].idBarang);

    return res.json({
      code: 200,
      status: 'success',
      message: 'success deleted one product',
      data: [],
    });
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .send({ code: 400, status: 'failed', message: error.message, data: [] });
  }
};

const productCont = { Add, Update, Delete };

export default productCont;
