import pool from '../config/db';

const Add = (data) => new Promise((resolve, reject) => {
  pool.query('INSERT INTO skripsi.invoices SET ?',
    data, (err, result) => ((!err) ? resolve(result) : reject(new Error(err))));
});

const getOne = (id) => new Promise((resolve, reject) => {
  pool.query('SELECT * FROM skripsi.invoices WHERE invoiceId = ? ORDER BY createdAt DESC',
    id, (err, result) => ((!err) ? resolve(result) : reject(new Error(err))));
});

const getAll = () => new Promise((resolve, reject) => {
  pool.query('SELECT * FROM skripsi.invoices ORDER BY createdAt DESC',
    (err, result) => ((!err) ? resolve(result) : reject(new Error(err))));
});

const Update = (data, id) => new Promise((resolve, reject) => {
  pool.query('UPDATE skripsi.invoices SET ? WHERE invoiceId = ?',
    [data, id], (err, result) => ((!err) ? resolve(result) : reject(new Error(err))));
});

const Delete = (id) => new Promise((resolve, reject) => {
  pool.query('DELETE FROM skripsi.invoices WHERE invoiceId = ?',
    id, (err, result) => ((!err) ? resolve(result) : reject(new Error(err))));
});

const modelSupplier = { Add, getOne, getAll, Update, Delete };

export default modelSupplier;
