import pool from '../config/db';

const Add = (data) => new Promise((resolve, reject) => {
  pool.query('INSERT INTO skripsi.supplier SET ?',
    data, (err, result) => ((!err) ? resolve(result) : reject(new Error(err))));
});

const getOne = (id) => new Promise((resolve, reject) => {
  pool.query('SELECT * FROM skripsi.supplier WHERE supplierId = ?',
    id, (err, result) => ((!err) ? resolve(result) : reject(new Error(err))));
});

const getAll = () => new Promise((resolve, reject) => {
  pool.query('SELECT * FROM skripsi.supplier ORDER BY createdAt DESC',
    (err, result) => ((!err) ? resolve(result) : reject(new Error(err))));
});

const Update = (data, id) => new Promise((resolve, reject) => {
  pool.query('UPDATE skripsi.supplier SET ? WHERE supplierId = ?',
    [data, id], (err, result) => ((!err) ? resolve(result) : reject(new Error(err))));
});

const Delete = (id) => new Promise((resolve, reject) => {
  pool.query('DELETE FROM skripsi.supplier WHERE supplierId = ?',
    id, (err, result) => ((!err) ? resolve(result) : reject(new Error(err))));
});

const modelSupplier = { Add, getOne, getAll, Update, Delete };

export default modelSupplier;
