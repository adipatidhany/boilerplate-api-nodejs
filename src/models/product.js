import pool from '../config/db';

const Add = (data) => new Promise((resolve, reject) => {
  pool.query('INSERT INTO skripsi.products SET ?', data, (err, result) => (!err ? resolve(result) : reject(new Error(err))));
});

const getOne = (id) => new Promise((resolve, reject) => {
  pool.query(
    'SELECT * FROM skripsi.products WHERE id = ? ORDER BY createdAt DESC',
    id,
    (err, result) => (!err ? resolve(result) : reject(new Error(err))),
  );
});

const isBarangExist = (invoiceId, barangId) => new Promise((resolve, reject) => {
  pool.query(
    'SELECT * FROM skripsi.products WHERE invoiceId = ? AND barangId = ? ORDER BY createdAt DESC',
    [invoiceId, barangId],
    (err, result) => (!err ? resolve(result) : reject(new Error(err))),
  );
});

const getInvoiceProduct = (id) => new Promise((resolve, reject) => {
  pool.query(
    'SELECT a.invoiceId, a.barangId, b.name, a.quantity \
    FROM skripsi.products a \
    LEFT JOIN skripsi.master_barang b \
    ON a.barangId = b.idBarang \
    WHERE invoiceId = ? \
    ORDER BY a.createdAt DESC',
    id,
    (err, result) => (!err ? resolve(result) : reject(new Error(err))),
  );
});

const Update = (data, id) => new Promise((resolve, reject) => {
  pool.query(
    'UPDATE skripsi.products SET ? WHERE id = ?',
    [data, id],
    (err, result) => (!err ? resolve(result) : reject(new Error(err))),
  );
});

const Delete = (id) => new Promise((resolve, reject) => {
  pool.query('DELETE FROM skripsi.products WHERE id = ?', id, (err, result) => (!err ? resolve(result) : reject(new Error(err))));
});

const modelSupplier = {
  Add,
  getOne,
  Update,
  Delete,
  getInvoiceProduct,
  isBarangExist,
};

export default modelSupplier;
