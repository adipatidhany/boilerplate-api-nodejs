import pool from '../config/db';

const Add = (data) => new Promise((resolve, reject) => {
  pool.query('INSERT INTO skripsi.master_barang SET ?', data, (err, result) => (!err ? resolve(result) : reject(new Error(err))));
});

const getOne = (id) => new Promise((resolve, reject) => {
  pool.query(
    'SELECT \
  a.id, \
  a.supplierId, \
  b.name as vendor, \
  a.idBarang, \
  a.name, \
  a.dimension, \
  a.quantity, \
  a.createdAt, \
  a.updatedAt \
  FROM skripsi.master_barang a \
  LEFT JOIN skripsi.supplier b ON a.supplierId = b.supplierId \
  WHERE a.idBarang = ?',
    id,
    (err, result) => (!err ? resolve(result) : reject(new Error(err))),
  );
});

const getAll = () => new Promise((resolve, reject) => {
  pool.query(
    'SELECT \
  a.id, \
  a.supplierId, \
  b.name as vendor, \
  a.idBarang, \
  a.name, \
  a.dimension, \
  a.quantity, \
  a.createdAt, \
  a.updatedAt \
  FROM skripsi.master_barang a \
  LEFT JOIN skripsi.supplier b ON a.supplierId = b.supplierId \
  ORDER BY a.createdAt DESC',
    (err, result) => (!err ? resolve(result) : reject(new Error(err))),
  );
});

const Update = (data, id) => new Promise((resolve, reject) => {
  pool.query(
    'UPDATE skripsi.master_barang SET ? WHERE idBarang = ?',
    [data, id],
    (err, result) => (!err ? resolve(result) : reject(new Error(err))),
  );
});

const Delete = (id) => new Promise((resolve, reject) => {
  pool.query(
    'DELETE FROM skripsi.master_barang WHERE idBarang = ?',
    id,
    (err, result) => (!err ? resolve(result) : reject(new Error(err))),
  );
});

const modelBarang = { Add, getOne, getAll, Update, Delete };

export default modelBarang;
