import pool from '../config/db';

const Add = (data) => new Promise((resolve, reject) => {
  pool.query('INSERT INTO skripsi.users SET ?', data, (err, result) => (!err ? resolve(result) : reject(new Error(err))));
});

const getOne = (email, username) => new Promise((resolve, reject) => {
  pool.query(
    'SELECT * FROM skripsi.users WHERE email = ? OR username = ?',
    [email, username],
    (err, result) => (!err ? resolve(result) : reject(new Error(err))),
  );
});

const authModel = { Add, getOne };

export default authModel;
