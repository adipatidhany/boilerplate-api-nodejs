import { createPool } from 'mysql';
import { promisify } from 'util';

require('dotenv').config();

const { host, user, password } = process.env;

const pool = createPool({
  host,
  user,
  password,
  connectionLimit: 10,
});

pool.getConnection((err, connection) => {
  if (err) {
    console.log(err);
  } else if (connection) {
    console.log('Database connected', host);
    connection.release();
  }
});

pool.query = promisify(pool.query);

export default pool;
