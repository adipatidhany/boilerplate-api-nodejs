import express from 'express';
import { json, urlencoded } from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import routerDev from './src/index';

require('dotenv').config();

const app = express();

const { port } = process.env;
app.listen(port, () => {
  console.log('Server listening on', port);
});

app.use(json());
app.use(urlencoded({ extended: false }));

app.use(morgan('dev'));
app.use(cors());

app.use('/', routerDev);

export default app;
