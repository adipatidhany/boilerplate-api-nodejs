-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.30-0ubuntu0.18.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for skripsi
CREATE DATABASE IF NOT EXISTS `skripsi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `skripsi`;

-- Dumping structure for table skripsi.invoices
CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceId` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT 'Pending',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table skripsi.invoices: ~3 rows (approximately)
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
INSERT INTO `invoices` (`id`, `invoiceId`, `name`, `keterangan`, `createdAt`, `updatedAt`) VALUES
	(4, 'INV003', 'UD Manunggal Jaya', 'Batal', '2020-07-25 20:28:29', '2020-07-25 20:28:29'),
	(6, 'INV002', 'UD. Menarik', 'Diterima', '2020-07-25 23:58:21', '2020-07-25 23:58:21'),
	(9, 'INV004', 'UD. SEJOLI', 'Pending', '2020-07-26 02:39:09', '2020-07-26 02:39:09');
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;

-- Dumping structure for table skripsi.master_barang
CREATE TABLE IF NOT EXISTS `master_barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplierId` varchar(255) DEFAULT NULL,
  `idBarang` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `dimension` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- Dumping data for table skripsi.master_barang: ~2 rows (approximately)
/*!40000 ALTER TABLE `master_barang` DISABLE KEYS */;
INSERT INTO `master_barang` (`id`, `supplierId`, `idBarang`, `name`, `dimension`, `quantity`, `createdAt`, `updatedAt`) VALUES
	(43, 'S0001', 'B0001', 'Pipa PVC', 'Diameter 20 Inch', 90, '2020-07-25 22:48:32', '2020-07-25 22:48:32'),
	(44, 'S0001', 'B0002', 'Kabel LTS', 'm', 90, '2020-07-26 00:38:30', '2020-07-26 00:38:30');
/*!40000 ALTER TABLE `master_barang` ENABLE KEYS */;

-- Dumping structure for table skripsi.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceId` varchar(255) DEFAULT NULL,
  `barangId` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table skripsi.products: ~4 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `invoiceId`, `barangId`, `quantity`, `createdAt`, `updatedAt`) VALUES
	(8, 'INV002', 'B0001', 10, '2020-07-26 00:58:52', '2020-07-26 00:58:52'),
	(9, 'INV002', 'B0002', 10, '2020-07-26 00:58:56', '2020-07-26 00:58:56'),
	(10, 'INV003', 'B0002', 10, '2020-07-26 00:59:01', '2020-07-26 00:59:01'),
	(11, 'INV003', 'B0001', 10, '2020-07-26 00:59:08', '2020-07-26 00:59:08');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table skripsi.supplier
CREATE TABLE IF NOT EXISTS `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplierId` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table skripsi.supplier: ~2 rows (approximately)
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` (`id`, `supplierId`, `name`, `vendor`, `createdAt`, `updatedAt`) VALUES
	(8, 'S0001', 'PT. Maju Bersama, Tbk', 'Germany', '2020-07-17 11:29:03', '2020-07-17 11:29:03'),
	(9, 'S0002', 'PT. Frankies', 'Korea', '2020-07-17 11:29:17', '2020-07-17 11:29:17');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;

-- Dumping structure for table skripsi.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` tinyint(4) DEFAULT '0',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table skripsi.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `email`, `salt`, `password`, `role`, `createdAt`, `updatedAt`) VALUES
	(17, 'franky', 'franky1', 'franky@gmail.com', '$2b$10$r6MhlCzhkT50x0l4SnkmWu', '$2b$10$r6MhlCzhkT50x0l4SnkmWuord//GdqaKzMkdU4kMuouPJAtIh1rXe', 0, '2020-07-16 16:33:16', '2020-07-16 16:33:16');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
